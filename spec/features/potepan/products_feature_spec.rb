RSpec.feature "Products_feature", type: :feature do
  let(:taxon) { create(:taxon, name: "hogehoge", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, name: "foobar", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, name: "Bag", price: "11.11", taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "Shirt", price: "77.77", taxons: [other_taxon]) }
  let!(:related_products) do
    5.times.collect do |i|
      create(:product, name: "related_product-#{i}",
                       price: "#{rand(1.0..99.9).round(2)}",
                       taxons: [taxon])
    end
  end

  before do
    visit potepan_product_path(product.id)
  end

  scenario "showページが正しく表示されること" do
    # メイン商品の表示
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_selector ".media-body h2", text: product.name
    expect(page).to have_selector ".media-body h3", text: product.display_price
    expect(page).to have_selector ".media-body p", text: product.description
    # 他の商品は表示しない
    expect(page).not_to have_selector ".media-body h2", text: other_product.name
    expect(page).not_to have_selector ".media-body h3", text: other_product.display_price

    # 関連商品の検証
    within ".productsContent" do
      expect(page).to have_selector ".productBox", count: 4
    end
    expect(page).to have_selector ".productCaption h5", text: related_products[0].name
    expect(page).to have_selector ".productCaption h3", text: related_products[0].display_price
    # 関連商品でない場合
    expect(page).not_to have_selector ".productCaption h5", text: other_product.name
    expect(page).not_to have_selector ".productCaption h3", text: other_product.display_price
    expect(page).not_to have_selector ".productCaption h5", text: product.name
    # ホームページへ移動する
    expect(page).to have_link "HOME", href: potepan_index_path
  end

  scenario "正しいリンク先に移動する" do
    click_link related_products[0].name
    expect(current_path).to eq potepan_product_path(related_products[0].id)

    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end
end
