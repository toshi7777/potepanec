RSpec.feature "Categories_feature", type: :feature do
  let(:taxon) { create(:taxon, name: "hogehoge", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, name: "foobar", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:product) { create(:product, name: "Bag", taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "Shirt", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "showページが正しく表示される" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    expect(page).to have_content "Bag"
    expect(page).not_to have_content "Shirt"
    expect(page).to have_link "HOME", href: potepan_index_path
  end

  scenario "正しいリンク先に移動すること" do
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)

    click_link product.display_price
    expect(current_path).to eq potepan_product_path(product.id)

    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(product.taxons.first.id)
  end

  scenario "商品カテゴリーが正しく表示されること" do
    within("#sidebar-category") do
      expect(page).to have_content "Brand"
      expect(page).to have_content "hogehoge (#{taxon.all_products.count})"
      expect(page).to have_content "foobar (#{other_taxon.all_products.count})"
    end
  end

  scenario "商品カテゴリーが正しいリンク先に移動すること" do
    within("#sidebar-category") do
      click_link "hogehoge"
      expect(current_path).to eq potepan_category_path(taxon.id)

      click_link "foobar"
      expect(current_path).to eq potepan_category_path(other_taxon.id)
    end
  end
end
