RSpec.describe "Products_request", type: :request do
  describe "GET prducts#show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "Showページのhttpリクエストは正しいか" do
      expect(response.status).to eq 200
    end

    it "Showページに商品名が表示されているか" do
      expect(response.body).to include product.name
    end

    it "Showページに商品価格が表示されているか" do
      expect(response.body).to include "#{product.display_price}"
    end

    it "Showページに商品の説明が表示されているか" do
      expect(response.body).to include product.description
    end

    it "関連商品のデータが取得できているか" do
      related_products.each do |related_product|
        expect(response.body).to include related_product.name
        expect(response.body).to include "#{related_product.display_price}"
      end
    end
  end
end
