RSpec.describe "Categories_request", type: :request do
  describe "GET categories#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "リクエストが成功する" do
      expect(response.status).to eq 200
    end

    context "商品カテゴリー" do
      it "taxonomyの名前が表示されること" do
        expect(response.body).to include taxonomy.name
      end

      it "taxonの名前が表示されること" do
        expect(response.body).to include taxon.name
      end
    end

    context "商品一覧" do
      it "商品の名前が表示されていること" do
        expect(response.body).to include product.name
      end

      it "商品の値段が表示されていること" do
        expect(response.body).to include "#{product.display_price}"
      end
    end
  end
end
