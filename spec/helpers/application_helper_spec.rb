RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    it "full_titleメソッドを表示する" do
      expect(full_title("title")).to eq "title - BIGBAG Store"
    end

    context "full_titleメソッドに空文字が渡された時" do
      it 'BIGBAG Storeのみ表示する' do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end

    context "full_titleメソッドにnilが渡された時" do
      it "BIGBAG Storeのみ表示する" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end
  end
end
